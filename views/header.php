<header class="masthead" style="background-image: url('img/home-bg.jpg')">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="site-heading">
              <h1>A&N Blog</h1>
              <span class="subheading">Hello Bloggers !!</span>
            </div>
          </div>
        </div>
      </div>
    </header>